## Building Docker Image

Run the following from the project root directory:

```
mvn package -DskipTests
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
docker build -t tote-crm .
```

## Running the app with Docker

```
docker run -p 9090:9090 -t totecrm
```

## Pushing image to AWS ECR

Make sure that awscli is installed and your AWS credentials are configured:

`aws configure`

To push the latest Docker image:

```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 009178328312.dkr.ecr.us-east-1.amazonaws.com/tote-crm
docker tag tote-crm:latest 009178328312.dkr.ecr.us-east-1.amazonaws.com/tote-crm:latest
docker push 009178328312.dkr.ecr.us-east-1.amazonaws.com/tote-crm:latest
```

To force the ECS cluster to grab the latest:

```
aws ecs update-service --cluster tote-crm --service tote-crm-rest --force-new-deployment
```
