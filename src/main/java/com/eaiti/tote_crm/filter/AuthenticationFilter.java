package com.eaiti.tote_crm.filter;

import com.eaiti.tote_crm.config.Constants;
import com.eaiti.tote_crm.security.AuthInfo;
import com.eaiti.tote_crm.security.AuthenticatedToken;
import com.eaiti.tote_crm.security.AuthenticationManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class AuthenticationFilter extends GenericFilterBean {

    private AuthenticationManagerImpl authenticationManager;

    @Autowired
    public AuthenticationFilter(AuthenticationManagerImpl authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        try {
            if (Arrays.asList(Constants.UNAUTHENTICATED_PATHS).stream().anyMatch(path -> httpRequest.getRequestURI().endsWith(path))) {
                chain.doFilter(request, response);
                return;
            }

            AuthenticatedToken authenticatedToken = null;
            if (SecurityContextHolder.getContext().getAuthentication() != null) {
                authenticatedToken = (AuthenticatedToken) SecurityContextHolder.getContext().getAuthentication();
            }

            AuthInfo authInfo = authenticatedToken != null ? authenticatedToken.getAuthInfo() : null;

            if (authInfo == null) {
                authenticatedToken = (AuthenticatedToken) SecurityContextHolder.getContext().getAuthentication();
            }

            if (authenticatedToken != null && !authenticatedToken.isAuthenticated()) {
                throw new BadCredentialsException("Auth Failed");
            }

            if (authenticatedToken != null) {
                SecurityContextHolder.clearContext();
                SecurityContextHolder.getContext().setAuthentication(authenticatedToken);
            }
            chain.doFilter(request, response);
        } catch (InternalAuthenticationServiceException internalAuthenticationServiceException) {
            httpResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            SecurityContextHolder.clearContext();
        } catch (AuthenticationException authenticationException) {
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, authenticationException.getMessage());
            SecurityContextHolder.clearContext();
        }
    }
}
