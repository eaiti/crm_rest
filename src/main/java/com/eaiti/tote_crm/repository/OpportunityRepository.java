package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Opportunity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OpportunityRepository extends JpaRepository<Opportunity, String> {

    @Query(value = "SELECT * FROM opportunities o WHERE" +
            "(:opportunityName IS NULL OR o.name LIKE %:opportunityName%) AND " +
            "(:accountId IS NULL OR :accountId IN (SELECT ao.account_id FROM accounts_opportunities ao WHERE ao.opportunity_id = o.id )) AND " +
            "(:contactId IS NULL OR :contactId IN (SELECT oc.contact_id FROM opportunities_contacts oc WHERE oc.opportunity_id = o.id )) AND " +
            "(:accountName IS NULL OR o.id IN (SELECT ao.opportunity_id FROM accounts_opportunities ao where ao.account_id IN (SELECT a.id FROM accounts a WHERE a.name LIKE %:accountName%))) AND " +
            "(:salesRepName IS NULL OR o.assigned_user_id IN (SELECT u.id FROM users u WHERE u.first_name LIKE %:salesRepName% OR u.last_name LIKE %:salesRepName%)) AND " +
            "( o.sales_stage IN ( :salesStages ) )", nativeQuery = true)
    Page<Opportunity> findOpportunities(@Param("opportunityName") final String opportunityName, @Param("salesRepName") final String salesRepName,
                                        @Param("accountId") final String accountId, @Param("contactId") final String contactId,
                                        @Param("accountName") final String accountName, @Param("salesStages") final List<String> salesStages, final Pageable pageable);

}
