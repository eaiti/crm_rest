package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface AccountRepository extends JpaRepository<Account, String> {

    @Query(value = "SELECT * FROM accounts a WHERE (:accountName IS NULL OR a.name LIKE %:accountName%) AND " +
            "(:contactId IS NULL OR :contactId IN (SELECT ac.contact_id FROM accounts_contacts ac WHERE ac.account_id = a.id )) AND " +
            "(:salesRepName IS NULL OR a.assigned_user_id IN (SELECT u.id FROM users u WHERE u.first_name LIKE %:salesRepName% OR u.last_name LIKE %:salesRepName%))", nativeQuery = true)
    Page<Account> findAccounts(@Param("accountName") final String accountName, @Param("contactId") String contactId, @Param("salesRepName") final String salesRepName, final Pageable pageable);

    @Query(value = "SELECT * FROM accounts a WHERE a.id IN (SELECT ac.account_id FROM accounts_contacts ac WHERE ac.contact_id = :contactId )", nativeQuery = true)
    List<Account> findAccountsByContactId(@Param("contactId") final String contactId);

    @Query(value = "SELECT * FROM accounts a WHERE a.id IN (SELECT ao.account_id FROM accounts_opportunities ao WHERE ao.opportunity_id = :opportunityId )", nativeQuery = true)
    List<Account> findAccountsByOpportunityId(@Param("opportunityId") final String opportunityId);
}
