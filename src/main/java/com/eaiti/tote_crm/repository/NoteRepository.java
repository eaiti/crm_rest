package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NoteRepository extends JpaRepository<Note, String> {
}
