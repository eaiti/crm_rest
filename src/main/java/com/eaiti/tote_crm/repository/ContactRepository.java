package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ContactRepository extends JpaRepository<Contact, String> {

    @Query(value = "SELECT * FROM contacts c WHERE (:contactName IS NULL OR c.last_name LIKE %:contactName% OR c.first_name LIKE %:contactName%) AND " +
            "(:accountId IS NULL OR :accountId IN (SELECT ac.account_id FROM accounts_contacts ac WHERE ac.contact_id = c.id ))", nativeQuery = true)
    Page<Contact> findContacts(@Param("contactName") final String contactName, @Param("accountId") String accountId, final Pageable pageable);
}
