package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Call;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CallRepository extends JpaRepository<Call, String> {
    Page<Call> findByParentId(final String parentId, final Pageable pageable);
}
