package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.Email;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Email, String> {
    Page<Email> findByParentId(final String parentId, final Pageable pageable);
}
