package com.eaiti.tote_crm.repository;

import com.eaiti.tote_crm.model.SalesRep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesRepRepository extends JpaRepository<SalesRep, String> {
}
