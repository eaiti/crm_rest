package com.eaiti.tote_crm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtil {
    final static String NO_USER = "no-user";

    public static String getUsername() {
        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            return auth != null ? auth.getName() : NO_USER;
        }
        return NO_USER;
    }
}

