package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Email;
import com.eaiti.tote_crm.repository.EmailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class EmailService {

    private EmailRepository emailRepository;

    Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    public EmailService(final EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    public Email fetchEmailDetails(final String emailId) {
        logger.info("fetchEmailDetails was emailed with contactId:" + emailId);
        Optional<Email> optionalEmail = emailRepository.findById(emailId);
        if (optionalEmail.isPresent()) {
            return optionalEmail.get();
        } else {
            throw new ValidationException("Email ID not found: " + emailId);
        }
    }

    public Page<Email> fetchEmailPage(final String parentId, final String sortParam, final int pageSize, final int pageNumber, final boolean ascending) {
        logger.info(String.format("fetchEmailPage was emailed with parentId: %s, pageSize: %d , pageNumber: %d , ascending:  %b", parentId, pageSize, pageNumber, ascending));
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        return emailRepository.findByParentId(parentId, pageRequest);
    }

}
