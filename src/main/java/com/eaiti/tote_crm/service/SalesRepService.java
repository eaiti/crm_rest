package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.SalesRep;
import com.eaiti.tote_crm.repository.SalesRepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SalesRepService {

    private SalesRepRepository salesRepRepository;


    @Autowired
    public SalesRepService(final SalesRepRepository salesRepRepository) {
        this.salesRepRepository = salesRepRepository;
    }

    public String fetchSalesRepName(final String salesRepId) {
        if (salesRepId == null) {
            return "";
        }
        final Optional<SalesRep> optionalSalesRep = salesRepRepository.findById(salesRepId);
        if (optionalSalesRep.isPresent()) {
            final SalesRep salesRep = optionalSalesRep.get();
            final String firstName = Optional.ofNullable(salesRep.getFirstName()).orElse("");
            final String lastName = Optional.ofNullable(salesRep.getLastName()).orElse("");
            return String.format("%s %s", firstName, lastName).trim();
        }
        return "";
    }
}
