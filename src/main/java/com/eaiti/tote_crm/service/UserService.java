package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.User;
import com.eaiti.tote_crm.repository.UserRepository;
import com.eaiti.tote_crm.security.AuthenticatedToken;
import com.eaiti.tote_crm.security.UserPasswordAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User getUser(final long id) {
        logger.info("getUser was called with id: " + id);
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new ValidationException("User ID not found: " + id);
        }
    }

    public User createUser(final User user) {
        logger.info("createUser was called with user: " + user);
        List<String> emptyList = new ArrayList<>();
        List<User> optionalUser = userRepository.findByUsername(user.getUsername());
        if (optionalUser.isEmpty()) {
            user.setDateEntered(new Date());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(user);
        } else {
            throw new ValidationException("Username already exists");
        }
    }

    public User findUserByUsername(final String username) {
        logger.info("findUserByUsername was called with username: " + username);
        List<User> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isEmpty()) {
            throw new ValidationException("Username not found: " + username);
        } else {
            return optionalUser.get(0);
        }
    }

    public void deleteLoggedInUser() {
        long currentUserId = getLoggedInUserId();
        logger.info("deleteLoggedInUser was called by id: " + currentUserId);
        Optional<User> optionalUser = userRepository.findById(currentUserId);
        if (optionalUser.isPresent()) {
            userRepository.delete(optionalUser.get());
        } else {
            throw new ValidationException("User ID not found: " + currentUserId);
        }
    }

    public void changePassword(final String newPassword) {
        long currentUserId = getLoggedInUserId();
        logger.info("changePassword was called by id: " + currentUserId);
        Optional<User> optionalUser = userRepository.findById(currentUserId);
        if (optionalUser.isPresent()) {
            User currentUser = optionalUser.get();
            currentUser.setPassword(passwordEncoder.encode(newPassword));
            userRepository.save(currentUser);
        } else {
            throw new ValidationException("The provided user ID does not match an existing user");
        }
    }

    private long getLoggedInUserId() {
        AuthenticatedToken authenticatedToken = (AuthenticatedToken) SecurityContextHolder.getContext().getAuthentication();
        long currentUserId = ((UserPasswordAuthentication) authenticatedToken.getCredentials()).getUser().getId();
        return currentUserId;
    }
}
