package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Call;
import com.eaiti.tote_crm.repository.CallRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class CallService {

    private CallRepository callRepository;

    Logger logger = LoggerFactory.getLogger(CallService.class);

    @Autowired
    public CallService(final CallRepository callRepository) {
        this.callRepository = callRepository;
    }

    public Call fetchCallDetails(final String callId) {
        logger.info("fetchCallDetails was called with contactId:" + callId);
        Optional<Call> optionalCall = callRepository.findById(callId);
        if (optionalCall.isPresent()) {
            return optionalCall.get();
        } else {
            throw new ValidationException("Call ID not found: " + callId);
        }
    }

    public Page<Call> fetchCallPage(final String parentId, final String sortParam,
                                    final int pageSize, final int pageNumber, final boolean ascending) {
        logger.info(String.format("fetchCallPage was called with parentId: %s, pageSize: %d , pageNumber: %d , ascending:  %b", parentId, pageSize, pageNumber, ascending));
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        return callRepository.findByParentId(parentId, pageRequest);
    }

}
