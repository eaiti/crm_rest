package com.eaiti.tote_crm.service;


import com.eaiti.tote_crm.model.Account;
import com.eaiti.tote_crm.model.Opportunity;
import com.eaiti.tote_crm.repository.AccountRepository;
import com.eaiti.tote_crm.repository.OpportunityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class OpportunityService {

    private OpportunityRepository opportunityRepository;
    private AccountRepository accountRepository;
    Logger logger = LoggerFactory.getLogger(OpportunityService.class);

    @Autowired
    public OpportunityService(final OpportunityRepository opportunityRepository,
                              final AccountRepository accountRepository) {
        this.opportunityRepository = opportunityRepository;
        this.accountRepository = accountRepository;
    }

    public Opportunity fetchOpportunityDetails(final String opportunityId) {
        final Optional<Opportunity> optionalOpportunity = opportunityRepository.findById(opportunityId);
        if (optionalOpportunity.isPresent()) {
            logger.info("Fetching opportunity page of matching: " + opportunityId);
            final Opportunity opportunity = optionalOpportunity.get();
            final List<Account> accounts = accountRepository.findAccountsByOpportunityId(opportunityId);
            opportunity.setRelatedAccounts(accounts);
            return opportunity;
        } else {
            throw new ValidationException("Opportunity ID not found: " + opportunityId);
        }
    }

    public Page<Opportunity> fetchOpportunityPage(final String opportunityName, final String salesRepName, final String accountId, final String contactId, final String accountName,
                                                  final List<String> salesStages, final String sortParam, final int pageSize, final int pageNumber, final boolean ascending) {
        final Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        List<String> resolvedSalesStages = salesStages;
        logger.info(String.format("fetchMeetingPage was called with salesRep: %s, pageSize: %d , pageNumber: %d , ascending: %b",
                salesRepName, pageSize, pageNumber, ascending));
        if (salesStages == null || salesStages.isEmpty()) {
            resolvedSalesStages = Arrays.asList("Closed Lost", "Prospecting", "Negotiating", "Closed Won", "Presenting", "Qualifying", "Target");
        }
        Page<Opportunity> opportunityPage = opportunityRepository.findOpportunities(opportunityName, salesRepName, accountId, contactId, accountName, resolvedSalesStages, pageRequest);
        opportunityPage.getContent().forEach(opportunity -> {
            final List<Account> accounts = accountRepository.findAccountsByOpportunityId(opportunity.getId());
            opportunity.setRelatedAccounts(accounts);
        });
        return opportunityPage;
    }

}
