package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Contact;
import com.eaiti.tote_crm.repository.AccountRepository;
import com.eaiti.tote_crm.repository.ContactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class ContactService {

    private ContactRepository contactRepository;
    private AccountRepository accountRepository;

    Logger logger = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    public ContactService(final ContactRepository contactRepository, final AccountRepository accountRepository) {
        this.contactRepository = contactRepository;
        this.accountRepository = accountRepository;
    }

    public Contact fetchContactDetails(final String contactId) {
        logger.info("fetchContactDetails was called with contactId:" + contactId);
        Optional<Contact> optionalContact = contactRepository.findById(contactId);
        if (optionalContact.isPresent()) {
            Contact contact = optionalContact.get();
            contact.setRelatedAccounts(accountRepository.findAccountsByContactId(contactId));
            return contact;
        } else {
            throw new ValidationException("Contact ID not found: " + contactId);
        }
    }

    public Page<Contact> fetchContactPage(final String contactName, final String accountId, final String sortParam,
                                          final int pageSize, final int pageNumber, final boolean ascending) {
        logger.info(String.format("fetchContactPage was called with contactName: %s, pageSize: %d, pageNumber: %d,  ascending: %b", contactName, pageSize, pageNumber, ascending));
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        Page<Contact> contactPage = contactRepository.findContacts(contactName, accountId, pageRequest);
        contactPage.getContent().forEach(contact -> {
            contact.setRelatedAccounts(accountRepository.findAccountsByContactId(contact.getId()));
        });
        return contactPage;
    }

}
