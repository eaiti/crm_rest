package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Note;
import com.eaiti.tote_crm.repository.NoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class NoteService {


    private NoteRepository meetingRepository;
    Logger logger = LoggerFactory.getLogger(NoteService.class);

    @Autowired
    public NoteService(final NoteRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public Note fetchNoteDetails(final String meetingId) {
        Optional<Note> optionalNote = meetingRepository.findById(meetingId);
        if (optionalNote.isPresent()) {
            logger.info("Fetching meeting page of matching: " + meetingId);
            return optionalNote.get();
        } else {
            throw new ValidationException("Note ID not found: " + meetingId);
        }
    }
}
