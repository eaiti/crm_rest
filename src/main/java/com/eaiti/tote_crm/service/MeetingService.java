package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Meeting;
import com.eaiti.tote_crm.repository.MeetingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class MeetingService {


    private MeetingRepository meetingRepository;
    Logger logger = LoggerFactory.getLogger(MeetingService.class);

    @Autowired
    public MeetingService(final MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public Meeting fetchMeetingDetails(final String meetingId) {
        Optional<Meeting> optionalMeeting = meetingRepository.findById(meetingId);
        if (optionalMeeting.isPresent()) {
            logger.info("Fetching meeting page of matching: " + meetingId);
            return optionalMeeting.get();
        } else {
            throw new ValidationException("Meeting ID not found: " + meetingId);
        }
    }

    public Page<Meeting> fetchMeetingPage(final String parentId, final String sortParam, final int pageSize, final int pageNumber, final boolean ascending) {
        logger.info(String.format("fetchMeetingPage was called with parentId: %s, pageSize: %d , pageNumber: %d , ascending:  %b", parentId, pageSize, pageNumber, ascending));
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        return meetingRepository.findByParentId(parentId, pageRequest);
    }
}
