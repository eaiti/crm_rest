package com.eaiti.tote_crm.service;

import com.eaiti.tote_crm.model.Account;
import com.eaiti.tote_crm.model.SalesRep;
import com.eaiti.tote_crm.repository.AccountRepository;
import com.eaiti.tote_crm.repository.SalesRepRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.Optional;

@Service
public class AccountService {


    private AccountRepository accountRepository;
    private SalesRepRepository salesRepRepository;
    Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    public AccountService(final AccountRepository accountRepository, final SalesRepRepository salesRepRepository) {
        this.accountRepository = accountRepository;
        this.salesRepRepository = salesRepRepository;
    }

    public Account fetchAccountDetails(String accountId) {
        final Optional<Account> optionalAccount = accountRepository.findById(accountId);
        if (optionalAccount.isPresent()) {
            logger.info("Fetching account page of matching: " + accountId);
            final Account account = optionalAccount.get();
            account.setSalesRepName(fetchSalesRepName(account.getAssignedUserId()));
            return account;
        } else {
            throw new ValidationException("Account ID not found: " + accountId);
        }
    }

    public Page<Account> fetchAccountPage(final String accountName, final String contactId, final String salesRepName, final String sortParam,
                                          final int pageSize, final int pageNumber, final boolean ascending) {
        final Pageable pageRequest = PageRequest.of(pageNumber, pageSize, ascending ? Sort.by(sortParam) : Sort.by(sortParam).descending());
        logger.info("Fetching account of id: " + accountName);
        final Page<Account> accountPage = accountRepository.findAccounts(accountName, contactId, salesRepName, pageRequest);
        accountPage.getContent().forEach(account -> account.setSalesRepName(
                fetchSalesRepName(account.getAssignedUserId())
        ));
        return accountPage;
    }

    private String fetchSalesRepName(final String salesRepId) {
        if (salesRepId == null) {
            return "";
        }
        final Optional<SalesRep> optionalSalesRep = salesRepRepository.findById(salesRepId);
        if (optionalSalesRep.isPresent()) {
            final SalesRep salesRep = optionalSalesRep.get();
            final String firstName = Optional.ofNullable(salesRep.getFirstName()).orElse("");
            final String lastName = Optional.ofNullable(salesRep.getLastName()).orElse("");
            return String.format("%s %s", firstName, lastName).trim();
        }
        return "";
    }

}
