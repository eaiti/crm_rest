package com.eaiti.tote_crm.controller;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<ErrorMessageList> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        ErrorMessageList errors = new ErrorMessageList(e.getBindingResult().getAllErrors().stream()
                .map(x -> new ErrorMessage(((FieldError) x).getField(), x.getDefaultMessage())).collect(Collectors.toList()));
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<ErrorMessageList> handleValidationException(ValidationException e) {
        ErrorMessage message = new ErrorMessage(null, e.getMessage());
        List<ErrorMessage> messageList = new ArrayList<ErrorMessage>();
        messageList.add(message);
        return new ResponseEntity<>(new ErrorMessageList(messageList), HttpStatus.BAD_REQUEST);
    }

    @Data
    static class ErrorMessage {
        String field;
        String errorMessage;

        ErrorMessage(String field, String errorMessage) {
            this.field = field;
            this.errorMessage = errorMessage;
        }
    }

    @Data
    static class ErrorMessageList {
        List<ErrorMessage> errors;

        public ErrorMessageList(List<ErrorMessage> errors) {
            this.errors = errors;
        }
    }
}
