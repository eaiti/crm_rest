package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.User;
import com.eaiti.tote_crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserResource {
    private UserService userService;

    @Autowired
    public UserResource(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseBody
    User createUser(@Valid @RequestBody User user) {
        return userService.createUser(user);
    }

    @DeleteMapping
    void deleteLoggedInUser() {
        userService.deleteLoggedInUser();
    }

    @PutMapping(value = "/password")
    void changePassword(@Valid @RequestParam String newPassword) {
        userService.changePassword(newPassword);
    }

    @GetMapping(value = "/{id}")
    User getUserById(@PathVariable("id") final long userId) {
        return userService.getUser(userId);
    }
}
