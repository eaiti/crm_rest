package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Contact;
import com.eaiti.tote_crm.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/contact")
public class ContactResource {

    private ContactService contactService;

    @Autowired
    public ContactResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping(value = "/{contactId}")
    Contact fetchContactDetails(@PathVariable final String contactId) {
        return contactService.fetchContactDetails(contactId);
    }

    @GetMapping
    Page<Contact> fetchContactPage(@RequestParam(required = false) final String contactName, @RequestParam(required = false) final String accountId,
                                   @RequestParam final String sortParam, @RequestParam final int pageSize,
                                   @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return contactService.fetchContactPage(contactName, accountId, sortParam, pageSize, pageNumber, ascending);
    }
}
