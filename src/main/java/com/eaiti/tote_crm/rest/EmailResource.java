package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Email;
import com.eaiti.tote_crm.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/email")
public class EmailResource {

    private EmailService emailService;

    @Autowired
    public EmailResource(final EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping(value = "/{emailId}")
    Email fetchEmailDetails(@PathVariable final String emailId) {
        return emailService.fetchEmailDetails(emailId);
    }

    @GetMapping
    Page<Email> fetchEmailPage(@RequestParam final String parentId, @RequestParam final String sortParam, @RequestParam final int pageSize,
                               @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return emailService.fetchEmailPage(parentId, sortParam, pageSize, pageNumber, ascending);
    }
}