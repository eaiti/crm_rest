package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Credentials;
import com.eaiti.tote_crm.model.User;
import com.eaiti.tote_crm.security.AuthenticatedToken;
import com.eaiti.tote_crm.security.AuthenticationManagerImpl;
import com.eaiti.tote_crm.security.UserPasswordAuthentication;
import com.eaiti.tote_crm.security.UserPasswordAuthenticationProvider;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
@RequestMapping("/auth")
public class AuthResource {

    private AuthenticationManagerImpl authenticationManager;

    public AuthResource(final AuthenticationManagerImpl authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    User login(@RequestBody Credentials credentials) {
        Authentication authenticatedToken = authenticationManager.authenticate(new UserPasswordAuthentication(credentials.getUsername(), credentials.getPassword()));
        if (authenticatedToken != null && authenticatedToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticatedToken);
            return ((UserPasswordAuthentication) authenticatedToken.getCredentials()).getUser();
        } else {
            throw new BadCredentialsException("Auth Failed");
        }
    }

    @PostMapping(value = "/logout")
    @ResponseBody
    void logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        SecurityContextHolder.clearContext();
    }
}
