package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Account;
import com.eaiti.tote_crm.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/account")
public class AccountResource {

    private AccountService accountService;

    @Autowired
    public AccountResource(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(value = "/{accountId}")
    Account fetchAccountDetails(@PathVariable final String accountId) {
        return accountService.fetchAccountDetails(accountId);
    }

    @GetMapping
    Page<Account> fetchAccountPage(@RequestParam(required = false) final String accountName, @RequestParam(required = false) final String contactId,
                                   @RequestParam(required = false) final String salesRepName, @RequestParam final String sortParam,
                                   @RequestParam final int pageSize, @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return accountService.fetchAccountPage(accountName, contactId, salesRepName, sortParam, pageSize, pageNumber, ascending);
    }
}
