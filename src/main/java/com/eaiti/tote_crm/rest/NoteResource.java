package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Note;
import com.eaiti.tote_crm.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/note")
public class NoteResource {

    private NoteService noteService;

    @Autowired
    public NoteResource(final NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping(value = "/{noteId}")
    Note fetchNoteDetails(@PathVariable final String noteId) {
        return noteService.fetchNoteDetails(noteId);
    }
}