package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Opportunity;
import com.eaiti.tote_crm.service.OpportunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/opportunity")
public class OpportunityResource {

    private OpportunityService opportunityService;

    @Autowired
    public OpportunityResource(final OpportunityService opportunityService) {
        this.opportunityService = opportunityService;
    }

    @GetMapping(value = "/{opportunityId}")
    Opportunity fetchOpportunityDetails(@PathVariable final String opportunityId) {
        return opportunityService.fetchOpportunityDetails(opportunityId);
    }

    @GetMapping
    Page<Opportunity> fetchOpportunityPage(@RequestParam(required = false) final String opportunityName, @RequestParam(required = false) final String salesRepName,
                                           @RequestParam(required = false) final String accountId, @RequestParam(required = false) final String contactId,
                                           @RequestParam(required = false) final String accountName,
                                           @RequestParam(required = false) final List<String> salesStages, @RequestParam final String sortParam,
                                           @RequestParam final int pageSize, @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return opportunityService.fetchOpportunityPage(opportunityName, salesRepName, accountId, contactId, accountName, salesStages, sortParam, pageSize, pageNumber, ascending);
    }
}