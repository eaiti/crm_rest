package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Meeting;
import com.eaiti.tote_crm.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/meeting")
public class MeetingResource {

    private MeetingService meetingService;

    @Autowired
    public MeetingResource(final MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    @GetMapping(value = "/{meetingId}")
    Meeting fetchMeetingDetails(@PathVariable final String meetingId) {
        return meetingService.fetchMeetingDetails(meetingId);
    }

    @GetMapping
    Page<Meeting> fetchMeetingPage(@RequestParam final String parentId, @RequestParam final String sortParam, @RequestParam final int pageSize,
                                   @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return meetingService.fetchMeetingPage(parentId, sortParam, pageSize, pageNumber, ascending);
    }
}