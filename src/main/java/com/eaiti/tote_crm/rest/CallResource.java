package com.eaiti.tote_crm.rest;

import com.eaiti.tote_crm.model.Call;
import com.eaiti.tote_crm.service.CallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/call")
public class CallResource {

    private CallService callService;

    @Autowired
    public CallResource(final CallService callService) {
        this.callService = callService;
    }

    @GetMapping(value = "/{callId}")
    Call fetchCallDetails(@PathVariable final String callId) {
        return callService.fetchCallDetails(callId);
    }

    @GetMapping
    Page<Call> fetchCallPage(@RequestParam final String parentId, @RequestParam final String sortParam, @RequestParam final int pageSize,
                             @RequestParam final int pageNumber, @RequestParam final boolean ascending) {
        return callService.fetchCallPage(parentId, sortParam, pageSize, pageNumber, ascending);
    }
}
