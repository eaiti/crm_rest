package com.eaiti.tote_crm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@JsonIgnoreProperties("emails")
@Entity(name = "email_addresses")
@Data
public class EmailAddress {
    @Id
    private String id;

    @Column
    private String emailAddress;

    @ManyToMany(mappedBy = "emailAddresses")
    private List<Email> emails;

}
