package com.eaiti.tote_crm.model;

import lombok.Data;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "accounts")
@Data
@EnableTransactionManagement
public class Account {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String accountType;

    @Column
    private String industry;

    @Column
    private String annualRevenue;

    @Column
    private String phoneFax;

    @Column
    private String billingAddressStreet;

    @Column
    private String billingAddressCity;

    @Column
    private String billingAddressState;

    @Column
    private String billingAddressPostalcode;

    @Column
    private String billingAddressCountry;

    @Column
    private String phoneOffice;

    @Column
    private String phoneAlternate;

    @Column
    private String website;

    @Column
    private String ownership;

    @Column
    private String employees;

    @Column
    private String shippingAddressStreet;

    @Column
    private String shippingAddressCity;

    @Column
    private String shippingAddressState;

    @Column
    private String shippingAddressPostalcode;

    @Column
    private String shippingAddressCountry;

    @Column
    private String assignedUserId;

    @Transient
    private String salesRepName;
}
