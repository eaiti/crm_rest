package com.eaiti.tote_crm.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "calls")
@Data
public class Call {
    @Id
    @Column
    private String id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Integer durationHours;

    @Column
    private Integer durationMinutes;

    @Column
    private Date dateStart;

    @Column
    private Date dateEnd;

    @Column
    private String status;

    @Column
    private String direction;

    @Column
    private String parentId;

    @Column
    private String outlookId;

    @Column
    private String assignedUserId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    private List<Note> noteList;
}
