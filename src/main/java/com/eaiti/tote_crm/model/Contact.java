package com.eaiti.tote_crm.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity(name = "contacts")
@Data
public class Contact {
    @Id
    @Column
    private String id;

    @Column
    private String description;

    @Column
    private String salutation;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String title;

    @Column
    private String department;

    @Column
    private boolean doNotCall;

    @Column
    private String phoneHome;

    @Column
    private String phoneMobile;

    @Column
    private String phoneWork;

    @Column
    private String phoneOther;

    @Column
    private String phoneFax;

    @Column
    private String primaryAddressStreet;

    @Column
    private String primaryAddressCity;

    @Column
    private String primaryAddressState;

    @Column
    private String primaryAddressPostalcode;

    @Column
    private String primaryAddressCountry;

    @Column
    private String altAddressStreet;

    @Column
    private String altAddressCity;

    @Column
    private String altAddressState;

    @Column
    private String altAddressPostalcode;

    @Column
    private String altAddressCountry;

    @Column
    private Date birthdate;

    @Column
    private String assignedUserId;

    @Transient
    private List<Account> relatedAccounts;
}
