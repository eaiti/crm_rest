package com.eaiti.tote_crm.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "meetings")
@Data
public class Meeting {

    @Id
    @Column
    private String id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String assignedUserId;

    @Column
    private String location;

    @Column
    private Integer durationHours;

    @Column
    private Integer durationMinutes;

    @Column
    private Date dateStart;

    @Column
    private Date dateEnd;

    @Column
    private String status;

    @Column
    private String type;

    @Column
    private String parentId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    private List<Note> noteList;
}
