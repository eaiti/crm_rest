package com.eaiti.tote_crm.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Entity(name = "opportunities")
@Data
public class Opportunity {
    @Id
    private String id;

    @Column
    private String name;

    @Column
    private Date dateEntered;

    @Column
    private String description;

    @Column
    private String opportunityType;

    @Column
    private String campaignId;

    @Column
    private String leadSource;

    @Column
    private Integer amount;

    @Column
    private Integer amountUsdollar;

    @Column
    private Integer dateClosedTimestamp;

    @Column
    private String nextStep;

    @Column
    private String salesStage;

    @Column
    private String salesStatus;

    @Column
    private Double probability;

    @Column
    private Integer bestCase;

    @Column
    private Integer worstCase;

    @OneToOne
    @JoinColumn(name = "assigned_user_id")
    private SalesRep salesRep;

    @Transient
    private List<Account> relatedAccounts;
}
