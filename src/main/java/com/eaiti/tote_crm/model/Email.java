package com.eaiti.tote_crm.model;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "emails")
@Data
public class Email {

    @Id
    private String id;

    @Column
    private String createdBy;

    @Column
    private Date dateSent;

    @Column
    private String name;

    @Column
    private String type;

    @Column
    private String status;

    @Column
    private String intent;

    @Column
    private String parentId;

    @Column
    private String state;

    @Column
    private String totalAttachments;

    @Column
    private String direction;

    @ManyToMany
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(
            name = "emails_email_addr_rel",
            joinColumns = @JoinColumn(name = "email_id"),
            inverseJoinColumns = @JoinColumn(name = "email_address_id")
    )
    private List<EmailAddress> emailAddresses;
}
