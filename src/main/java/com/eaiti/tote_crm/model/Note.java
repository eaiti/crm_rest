package com.eaiti.tote_crm.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "notes")
@Data
public class Note {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private Date dateEntered;

    @Column
    private Date dateModified;

    @Column
    private String description;

    @Column
    private String assignedUserId;

    @Column(name = "parent_id", insertable = false, updatable = false)
    private String parentId;

    @Column
    private String contactId;

    @Column
    private String emailType;

    @Column
    private String emailId;
}
