package com.eaiti.tote_crm.model;

import lombok.Data;

@Data
public class Credentials {
    private String username;
    private String password;
}
