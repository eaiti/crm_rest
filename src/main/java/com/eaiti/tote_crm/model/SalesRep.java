package com.eaiti.tote_crm.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "users")
@Data
public class SalesRep {
    @Id
    @Column
    private String id;

    @Column
    private String userName;

    @Column
    private String userHash;

    @Column
    private boolean systemGeneratedPassword;

    @Column
    private Date pwdLastChanged;

    @Column
    private String authenticateId;

    @Column
    private boolean sugarLogin;

    @Column
    private String picture;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private boolean isAdmin;

    @Column
    private boolean externalAuthOnly;

    @Column
    private boolean receiveNotifications;

    @Column
    private String description;

    @Column
    private Date dateEntered;

    @Column
    private Date dateModified;

    @Column
    private Date lastLogin;

    @Column
    private String modifiedUserId;

    @Column
    private String createdBy;

    @Column
    private String title;

    @Column
    private String department;

    @Column
    private String phoneHome;

    @Column
    private String phoneMobile;

    @Column
    private String phoneWork;

    @Column
    private String phoneOther;

    @Column
    private String phoneFax;

    @Column
    private String status;

    @Column
    private String addressStreet;

    @Column
    private String addressCity;

    @Column
    private String addressState;

    @Column
    private String addressCountry;

    @Column
    private String addressPostalcode;

    @Column
    private String licenseType;

    @Column
    private String defaultTeam;

    @Column
    private String teamSetId;

    @Column
    private boolean deleted;

    @Column
    private boolean portalOnly;

    @Column
    private boolean showOnEmployees;

    @Column
    private String employeeStatus;

    @Column
    private String messengerId;

    @Column
    private String messengerType;

    @Column
    private String reportsToId;

    @Column
    private boolean isGroup;

    @Column
    private String preferredLanguage;

    @Column
    private String aclRoleSetId;

    @Column
    private String siteUserId;
}
