package com.eaiti.tote_crm.security;

import com.eaiti.tote_crm.model.User;
import com.eaiti.tote_crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
public class UserPasswordAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        UserPasswordAuthentication upAuthentication = (UserPasswordAuthentication) authentication;
        User user = login(upAuthentication.getUsername(), upAuthentication.getPassword());
        if (user != null) {
            return new UserPasswordAuthentication(user);
        }

        throw new BadCredentialsException("Auth Failed");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication != null;
    }

    public User login(String username, String password) {
        User user = userService.findUserByUsername(username);
        if (passwordEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new ValidationException("Wrong password!");
        }
    }
}
