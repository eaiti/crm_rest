package com.eaiti.tote_crm.security;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

@EqualsAndHashCode(callSuper = true)
@Data
public class AuthenticatedToken extends PreAuthenticatedAuthenticationToken {
    private AuthInfo authInfo;

    AuthenticatedToken(Object aPrincipal, Object aCredentials) {
        super(aPrincipal, aCredentials);
    }

    AuthenticatedToken(Authentication authentication) {
        super(authentication.getPrincipal(), authentication.getCredentials(), authentication.getAuthorities());
        if (authentication.isAuthenticated()) {
            if (authentication instanceof UserPasswordAuthentication) {
                this.authInfo = new AuthInfo();
            }
        }
        if (this.authInfo == null) {
            throw new BadCredentialsException("Auth failed");
        }
    }

    @Override
    public String getName() {
        return authInfo != null ? authInfo.getUsername() : "";
    }

    @Override
    public boolean isAuthenticated() {
        return super.isAuthenticated();
    }

}
