package com.eaiti.tote_crm.security;

import com.eaiti.tote_crm.model.User;
import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class UserPasswordAuthentication implements Authentication {
    private String userId;
    private String username;
    private String password;
    private User user;
    private boolean authenticated = false;

    public UserPasswordAuthentication(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserPasswordAuthentication(User user) {
        this.user = user;
    }


    @Override
    public UserPasswordAuthentication getCredentials() {
        return this;
    }

    @Override
    public User getDetails() {
        return user;
    }

    @Override
    public Object getPrincipal() {
        return username;
    }

    @Override
    public boolean isAuthenticated() {
        return user != null;
    }

    @Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
        this.authenticated = authenticated;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (isAuthenticated()) {
            authorities.add(new SimpleGrantedAuthority("ALL_PRIVILEGES"));
        }
        return authorities;
    }

    @Override
    public String getName() {
        return "Username|Password";
    }

}
