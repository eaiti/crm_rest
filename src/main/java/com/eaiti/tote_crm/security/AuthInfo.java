package com.eaiti.tote_crm.security;

import com.eaiti.tote_crm.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthInfo {
    private String userId;
    private String username;
    private String firstName;
    private String lastName;
    private String email;

    public AuthInfo(User user) {
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
    }

    public String getUsername() {
        return username;
    }
}
