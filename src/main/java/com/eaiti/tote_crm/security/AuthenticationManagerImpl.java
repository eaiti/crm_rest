package com.eaiti.tote_crm.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationManagerImpl {

    private AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationManagerImpl(final AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public AuthenticatedToken authenticate(UserPasswordAuthentication authentication) {
        UserPasswordAuthentication authenticatedAuth = (UserPasswordAuthentication) authenticationManager.authenticate(authentication);
        return new AuthenticatedToken(authenticatedAuth);
    }
}
