FROM openjdk:8-jdk-alpine

ARG DEPENDENCY=target/dependency

COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib

COPY ${DEPENDENCY}/META-INF /app/META-INF

COPY ${DEPENDENCY}/BOOT-INF/classes /app

COPY logs app/logs

ENV LOG_PATH /app/logs

ENTRYPOINT ["java","-cp","app:app/lib/*","com.eaiti.tote_crm.ToteCrmApplication"]